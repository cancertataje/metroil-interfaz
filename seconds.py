import days

def count(A):
    A=str(A).split("         ")
    B=A[1].split("-")
    C,D,E=days.count(B[0])
    F=B[1].split(":")
    HOURS=int(F[0])
    MINUTES=int(F[1])
    SECONDS=int(F[2])
    DAY=C
    MONTH=D
    YEAR=E
    return DAY, MONTH, YEAR, HOURS, MINUTES, SECONDS
    
def count2(A):
    A=str(A).split("         ")
    B=A[1].split("-")
    fecha=days.date(B[0])
    return fecha

def count3(A):
    A=str(A).split("         ")
    B=A[1].split("-")
    C=str(B[1]).split(':')
    D=(int(C[0])*60+int(C[1]))/5

    return int(D)

def count4(A):
    A=str(A).split("         ")
    B=A[1].split("-")
    C=B[1].split(":")
    D=C[0]+":"+C[1]+":00"
    return B[0], D 
