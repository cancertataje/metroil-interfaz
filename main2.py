#'-------------------------------------------------------------------------------
#'This software has been developed by the IT department belonging to METROIL S.A.C.
#'-------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------
#'Reservados todos los derechos. No se permite la reproducción total o parcial de
#'esta obra, ni su incorporación a un sistema informático, ni su transmisión en
#'cualquier forma o por cualquier medio (electrónico, mecánico, fotocopia, grabación
#'u otros) sin autorización previa y por escrito de los titulares del copyright. La
#'infracción de dichos derechos puede constituir un delito contra la propiedad intelectual.
#'copyright © METROIL S.A.C., 2022
#'-------------------------------------------------------------------------------


#'-------------------------------------------------------------------------------
#'--Code            :   010422
#'--Date            :   01/04/22
#'--User            :   Paolo G. Tataje
#'--Description     :   Creation of Interfaz
#'--Status          :   Complete
#'-------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------
#'--Code            :   020422
#'--Date            :   02/04/22
#'--User            :   Paolo G. Tataje
#'--Description     :   Creation of local database
#'--Status          :   complete
#'-------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------
#'--Code            :   030422
#'--Date            :   03/04/22
#'--User            :   Paolo G. Tataje
#'--Description     :   Creation of graph
#'--Status          :   complete
#'-------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------
#'--Code            :   100622
#'--Date            :   10/06/22
#'--User            :   Paolo G. Tataje
#'--Description     :   Option for add more excels 
#'--Status          :   complete
#'-------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------
#'--Code            :   110622
#'--Date            :   11/06/22
#'--User            :   Paolo G. Tataje
#'--Description     :   Selection of dates
#'--Status          :   complete
#'-------------------------------------------------------------------------------


import sys
import pyqtgraph as pg
import openpyxl
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.drawing.image import Image
from openpyxl.styles import Alignment, GradientFill, Border, Side
from openpyxl.formatting.rule import DataBarRule
from openpyxl.styles import numbers
from datetime import date

import locale
from matplotlib import pyplot as plt
import matplotlib.dates as dates
from pylab import *
import os 

from datetime import datetime, timedelta
import datetime

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtSql import *
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QFont
from PyQt5.QtWidgets import (QFileDialog, QApplication, qApp, QMainWindow, QDialog,
                             QPushButton, QLabel, QMessageBox, QTextEdit, QComboBox,
                             QCheckBox, QMessageBox, QWidget, QAction, QTabWidget,
                             QVBoxLayout, QMenuBar, QGridLayout, QStatusBar, QRadioButton,
                             QStyle, QActionGroup, QFrame, QStackedWidget, QWidget,
                             QTableWidget, QTableWidgetItem, QProgressBar, QCalendarWidget)
from PyQt5.QtCore import QCoreApplication, Qt, QByteArray, QFile, QByteArray, QIODevice, QBuffer, QModelIndex, QDate

import seconds
import days
import Average
import Gradiente

import calendar
import xlsxwriter


#-------------- Code: 110622 --------------#

class Ui_Calendario(QDialog):    
    def __init__(self, parent=None):
        super(Ui_Calendario, self).__init__(None)

        self.parent = parent
        self.setWindowTitle("Selección de Fechas")
        self.setWindowIcon(QIcon("0.ico"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(550, 300)
        
        self.initUI()
        
    def initUI(self):
        self.calendar = QCalendarWidget(self)
        self.calendar.move(20, 20)

        self.calendar.setMinimumDate(QDate(2022, 6-1, 1))
        self.calendar.setMaximumDate(QDate(2022, 6+1, 1))

        self.pushButton_inicio = QPushButton(self)
        self.pushButton_inicio.setText("Inicio")
        self.pushButton_inicio.setGeometry(430,75,100,25)

        self.pushButton_fin = QPushButton(self)
        self.pushButton_fin.setText("Fin")
        self.pushButton_fin.setGeometry(430,150,100,25)

        maxdato = maxdate
        maxdeto = str(maxdato).split(' ')        
        maxdato = str(maxdeto[0]).split('-')

        self.label_fin = QLabel(self)
        self.label_fin.setText(maxdeto[0])
        self.label_fin.setGeometry(445,185,100,25)

        

        mindato = mindate
        mindeto = str(mindato).split(' ')        
        mindato = str(mindeto[0]).split('-')

        self.label_inicio = QLabel(self)
        self.label_inicio.setText(mindeto[0])
        self.label_inicio.setGeometry(445,110,100,25)

    

        self.calendar.setMinimumDate(QDate(int(mindato[0]), int(mindato[1]), int(mindato[2])))
        self.calendar.setMaximumDate(QDate(int(maxdato[0]), int(maxdato[1]), int(maxdato[2])))

        try:            
            self.label_inicio.setText(minvalue)

        except:        
            pass
        try:
            self.label_fin.setText(maxvalue)

        except:
            pass
        
        self.pushButton_inicio.clicked.connect(self.inicio)
        self.pushButton_fin.clicked.connect(self.fin)

    def inicio(self):
        global minvalue
        
        value = self.calendar.selectedDate()
        value = str(value.toPyDate())

        maxvalue = self.label_fin.text()

        
        if (datetime.datetime.strptime(value, '%Y-%m-%d') >= datetime.datetime.strptime(maxvalue, '%Y-%m-%d')):
            mesa="La fecha de inicio no puede ser después o igual que la fecha final"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
        else:
            self.label_inicio.setText(value)
            minvalue = value

            

            


    def fin(self):
        global maxvalue
        
        value = self.calendar.selectedDate()        
        value = str(value.toPyDate())

        minvalue = self.label_inicio.text()

        
        if (datetime.datetime.strptime(minvalue, '%Y-%m-%d') >= datetime.datetime.strptime(value, '%Y-%m-%d')):
            mesa="La fecha de final no puede ser anterior o igual que la fecha inicial"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
        else:
            self.label_fin.setText(value)
            maxvalue = value
            
    def mousePressEvent(self, event):
        print("button")
        print(event.button())
        print("Leftbutton")
        print(Qt.LeftButton)
        if event.button() == Qt.LeftButton:
            print("---")



#-------------- Code: 010422 --------------#

class Ui_Acerca(QDialog):
    def __init__(self, parent=None):
        super(Ui_Acerca, self).__init__(None)
        
        self.parent=parent

        self.setWindowTitle("Acerca de...")
        self.setWindowIcon(QIcon("0.ico"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(660, 300)

        self.initUI()

    def initUI(self):
        
        self.label= QLabel(self)
        font = QtGui.QFont()
        font.setBold(True)        
        font.setWeight(75)
        font.setFamily("Century")
        font.setPointSize(15)
        self.label.setFont(font)        
        self.label.setText("Data Analytics - Controller")
        self.label.setGeometry(110, 40, 400, 51)

        self.label_2= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_2.setFont(font)        
        self.label_2.setText("El presente software ha sido elaborado por el departamento")
        self.label_2.setGeometry(25, 80, 550, 51)

        self.label_3= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_3.setFont(font)        
        self.label_3.setText("de Tecnología de la Información perteneciente a METROIL")
        self.label_3.setGeometry(25, 100, 550, 51)

        self.label_4= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_4.setFont(font)        
        self.label_4.setText("S.A.C.")
        self.label_4.setGeometry(25, 120, 500, 51)

        self.label_5= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_5.setFont(font)        
        self.label_5.setText("Dirección: Av. República de Venezuela 2040 - Lima 1, Lima - Perú")
        self.label_5.setGeometry(25, 160, 620, 51)

        self.label_6= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_6.setFont(font)        
        self.label_6.setText("Teléfono: +51 943 610 027")
        self.label_6.setGeometry(25, 190, 500, 51)

        self.label_7= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_7.setFont(font)        
        self.label_7.setText("Correo: ventas@metroil.pe")
        self.label_7.setGeometry(25, 220, 500, 51)
        

#-------------- Code: 010422 --------------#

APP_CLASS, PARENT_CLASS = uic.loadUiType("Controler.ui")

class MyApp(PARENT_CLASS, APP_CLASS):
    def __init__(self):        
        PARENT_CLASS.__init__(self)
        APP_CLASS.__init__(self)
        self.setupUi(self)

        self.table = QTableWidget(0, 9)
        self.table.setHorizontalHeaderLabels(['Epoch Time', 'Log Time', 'Vol.  1 - Level (m)', 'Vol.  1 - Volume (m3)', 'OCM 1 - Head (m)', 'OCM 1 - Flow (litres/sec)', 'OCM 1 - Dly Totaliser (m3)', 'OCM 1 - Sys Totaliser (m3)', 'OCM 1 - Rst Totaliser (m3)']) 
        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection)
        self.table.horizontalHeader().setDefaultSectionSize(190)
        self.gridLayout_3.addWidget(self.table)


        self.pushButton_generar.setEnabled(False)

        self.pushButton_fechas.setEnabled(False)

        self.progressBar.setMinimum(0)
        self.progressBar.hide()

        pixmap = QPixmap('metroil.png')
        self.label_Imag.setPixmap(pixmap)

        self.tab_2.layout = QtWidgets.QGridLayout()

        self.w = pg.PlotWidget(axisItems={'bottom': pg.DateAxisItem()})
        self.w.showGrid(x=True, y=True)
                
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)


        self.w.scene().sigMouseMoved.connect(self.mouseMoved)
        self.tab_2.layout.addWidget(self.w,7,0,1,5)

        self.label_titlegrap=QLabel()
        self.label_titlegrap.setText("")
        self.label_titlegrap.setStyleSheet("color: gray;")
        self.tab_2.layout.addWidget(self.label_titlegrap,2,0,1,5)
        
        self.label_3 = QLabel()        
        self.label_3.setText("Eje X :")
        self.tab_2.layout.addWidget(self.label_3,0,0,1,1)

        self.label_4 = QLabel()        
        self.label_4.setText("Eje Y :")
        self.tab_2.layout.addWidget(self.label_4,1,0,1,1)

        self.comboBox_1 = QComboBox()        
        self.comboBox_1.addItem("Log Time")
        self.tab_2.layout.addWidget(self.comboBox_1,0,1,1,1)

        self.comboBox_2 = QComboBox()        
        self.comboBox_2.addItem("Vol. 1 - Level (m)")
        self.comboBox_2.addItem("Vol. 1 - Volume (m3)")
        self.comboBox_2.addItem("OCM 1 - Head (m)")
        self.comboBox_2.addItem("OCM 1 - Flow (litres/sec)")
        self.comboBox_2.addItem("OCM 1 - Dly Totaliser (m3)")
        self.comboBox_2.addItem("OCM 1 - Sys Totaliser (m3)")
        self.comboBox_2.addItem("OCM 1 - Rst Totaliser (m3)")
        self.tab_2.layout.addWidget(self.comboBox_2,1,1,1,1)

        self.pushButton_2=QPushButton()
        self.pushButton_2.setText("Graficar")
        self.pushButton_2.setEnabled(False)
        self.pushButton_2.clicked.connect(self.graf)
        self.tab_2.layout.addWidget(self.pushButton_2,0,2,1,1)
        
        self.tab_2.setLayout(self.tab_2.layout)
        
        self.Insertar = self.Archivo.addAction(self.style().standardIcon(
            QStyle.SP_FileDialogContentsView), "Insertar Archivos", self.Opens)

        self.Nuevo = self.Archivo.addAction(self.style().standardIcon(
            QStyle.SP_FileIcon), "Nuevo", self.Nuev)
        
        self.Archivo.addSeparator()
        
        self.Salir = self.Archivo.addAction(self.style().standardIcon(
            QStyle.SP_TitleBarCloseButton), "Salir", self.close)        
        
        self.Software = self.Acerca.addAction(self.style().standardIcon(
            QStyle.SP_MessageBoxInformation), "Software", self.Acer)        

        self.pushButton_generar.setStyleSheet("background-color : #00ff26")
        self.pushButton_generar.clicked.connect(self.generar)

        self.pushButton_fechas.clicked.connect(self.fecha)

    def generar(self):
        try:

            CantRow = int(self.table.rowCount())

            wb = openpyxl.Workbook()
            sheet = wb.active
            
            c1 = sheet.cell(row = 1, column = 1) 
            c1.value = "Epoch Time"

            c2 = sheet.cell(row = 1, column = 2) 
            c2.value = "Log Time"

            c3 = sheet.cell(row = 1, column = 3) 
            c3.value = "Vol. 1 - Level (m)"

            c4 = sheet.cell(row = 1, column = 4) 
            c4.value = "Vol. 1 - Volume (m3)"

            c5 = sheet.cell(row = 1, column = 5) 
            c5.value = "OCM 1 - Head (m)"

            c6 = sheet.cell(row = 1, column = 6) 
            c6.value = "OCM 1 - Flow (litres/sec)"

            c7 = sheet.cell(row = 1, column = 7) 
            c7.value = "OCM 1 - Dly Totaliser (m3)"

            c8 = sheet.cell(row = 1, column = 8) 
            c8.value = "OCM 1 - Sys Totaliser (m3)"

            c9 = sheet.cell(row = 1, column = 9) 
            c9.value = "OCM 1 - Rst Totaliser (m3)"

            for i in range(0,CantRow):
                cx = sheet.cell(row = i+2, column = 1) 
                cx.value = float(self.table.item(i,0).text())

                cy = sheet.cell(row = i+2, column = 2) 
                cy.value = self.table.item(i,1).text()

                cz = sheet.cell(row = i+2, column = 3) 
                cz.value = float(self.table.item(i,2).text())

                ca = sheet.cell(row = i+2, column = 4) 
                ca.value = float(self.table.item(i,3).text())

                cb = sheet.cell(row = i+2, column = 5) 
                cb.value = float(self.table.item(i,4).text())

                cc = sheet.cell(row = i+2, column = 6) 
                cc.value = float(self.table.item(i,5).text())

                cd = sheet.cell(row = i+2, column = 7) 
                cd.value = float(self.table.item(i,6).text())

                ce = sheet.cell(row = i+2, column = 8) 
                ce.value = float(self.table.item(i,7).text())

                cf = sheet.cell(row = i+2, column = 9) 
                cf.value = float(self.table.item(i,8).text())

            name = QFileDialog.getSaveFileName(None, caption='Select a data file', filter='Archivos de Excel (*.xlsx)')            
            wb.save(name[0])

            wb2 = openpyxl.Workbook()
            sheet = wb2.active

            List = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA',
                    'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX',
                    'AY', 'AZ', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU']
            
            for i in range(0,len(List)):
                sheet.column_dimensions[List[i]].width = 12

            thin = Side(border_style="thin", color="FFFFFF") #color="CCFFFF"
            border = Border(left=thin, right=thin, top=thin, bottom=thin)
                
            ct = sheet.cell(row = 1, column = 10)
            ct.value = 'REPORTE DE CAUDALES A DETALLE'
            

            ct = sheet.cell(row = 1, column = 1)
            ct.value = 'Promedio de Etiquetas'
            fille = GradientFill(stop=("CCCCCC", "CCCCCC"))            
            ct.fill = fille
            ct.border = border

            ct = sheet.cell(row = 2, column = 1)
            ct.value = 'Etiquetas'          
            ct.fill = fille
            ct.border = border
            
            nomb1 = str(name[0]).split("/")
            nomb = nomb1[len(nomb1)-1].replace('.xlsx',' - reporte.xlsx')
            nomb2 = ""
            
            for i in range(0,len(nomb1)-1):
                nomb2 = str(nomb2) + str(nomb1[i]) + "/"
            nombre = nomb2 + str(nomb)

            fechas = np.zeros(CantRow, dtype = object)
            horawmin = np.zeros(CantRow, dtype = object)
            Datos = np.zeros(CantRow)

            for i in range(0,CantRow):
                fechas[i] = seconds.count2(str(self.table.item(i,1).text()))
                horawmin[i] = seconds.count3(str(self.table.item(i,1).text()))
                Datos[i] = float(self.table.item(i,5).text())

            

            try:
                Fecha_cero = minvalue
                Fecha_cero = str(Fecha_cero).split('-')
                Fecha_cero = Fecha_cero[2]+"/"+Fecha_cero[1]+"/"+Fecha_cero[0]

            except:
                Fecha_cero = fechas[0]
                
            try:
                Fecha_fin = maxvalue               
                Fecha_fin = str(Fecha_fin).split('-')
                Fecha_fin = Fecha_fin[2]+"/"+Fecha_fin[1]+"/"+Fecha_fin[0]
    
            except:
                Fecha_fin = fechas[len(fechas)-1]

            
            #---------------------------- Estructura para hacer fechas consecutivas ----------------------------#
                
            date_format = "%d/%m/%Y"
            start_date = datetime.datetime.strptime(Fecha_cero, date_format)
            end_date = datetime.datetime.strptime(Fecha_fin, date_format)

            delta = datetime.timedelta(days=1)

            result = []
            while start_date <= end_date:
                result.append(start_date.strftime(date_format))
                start_date += delta

            result_str = ", ".join(result)

            
            for i in range(0, len(result)):

                cf = sheet.cell(row = 2, column = 2+i)
                cf.alignment = Alignment(horizontal="center",
                                          vertical="center")
                cf.fill = fille
                cf.border = border
                cf.value = result[i]

                cf = sheet.cell(row = 1, column = 2+i)
                cf.fill = fille

            cf = sheet.cell(row = 2, column = 2+len(result))
            cf.value = 'Total generados'
            cf.alignment = Alignment(horizontal="center",
                                      vertical="center")
            cf.fill = fille
            cf.border = border

            cf = sheet.cell(row = 1, column = 2+len(result))
            cf.fill = fille

            for i in range(0,288): #----- 24 hours multiplicate for 60 minutes, dividide between 5 minutes
                now = timedelta(minutes = 5*i)
                cti = sheet.cell(row = 3+i, column = 1)
                cti.value = now

            
            sheet.auto_filter.ref = "A2"

            
            #----------------------------------------------------------------------------------------------------#

            Matriz_Datos = np.zeros([288,len(result)])
            NumRow = len(Matriz_Datos[:,0])
            NumCol = len(Matriz_Datos[0,:])

            cf = sheet.cell(row = 3+NumRow, column = 1)
            cf.value = 'Total generados'
            cf.alignment = Alignment(horizontal="center",
                                      vertical="center")
            cf.fill = fille
            cf.border = border

            #-------------------------- Sorting matrix --------------------------#
            for i in range(0,len(Datos)):
                if (float(Datos[i]) == 0):
                    Datos[i] = -10000

            
            print("Test1")
            
            try:
                
                j = 0
                for z in range(0, len(fechas)):
                    try:
                        index = result.index(fechas[z])                    
                        Matriz_Datos[horawmin[j],index] = Datos[j]
                        j += 1
                    except:
                        j += 1
                        pass
                               
            except Exception as e:
                print(e)
                pass

            
            NumRow = len(Matriz_Datos[:,0])            
            NumCol = len(Matriz_Datos[0,:])
            
            for i in range(0,NumRow):
                for j in range(0,NumCol):
                    cti = sheet.cell(row = 3+i, column = 2+j)
                    if Matriz_Datos[i][j] == 0:
                        cti.value = ''
                    elif Matriz_Datos[i][j] == -10000:
                        cti.value = 0
                    else:
                        cti.value = Matriz_Datos[i][j]

            print("Test2")

            #'------------- Put formula -------------'#
                        
            for i in range(0,NumRow):
                cti = sheet.cell(row = 3+i, column = 2+NumCol)
                value1 = '=AVERAGE(B'+str(3+i)+':'
                value2 = str(get_column_letter(1+NumCol))
                valuef = value1+value2+str(3+i)+')'                    
                cti.value = valuef

            for i in range(0,NumCol):
                cti = sheet.cell(row = 3+NumRow, column = 2+i)
                value1 = str(get_column_letter(2+i))
                valuef = '=AVERAGE('+value1+str(3)+':'+value1+str(2+NumRow)+')'                
                cti.value = valuef

            cti = sheet.cell(row = 3+NumRow, column = 2+NumCol)
            value1 = '=AVERAGE(B'+str(3+NumRow)+':'
            value2 = str(get_column_letter(1+NumCol))
            valuef = value1+value2+str(3+NumRow)+')'
            cti.value = valuef

            for i in range(0,NumRow):
                for j in range(0,NumCol):
                    if Matriz_Datos[i][j] == -10000:
                        Matriz_Datos[i][j] = 0

            print("Test3")

            rule = DataBarRule(start_type = 'num', start_value=np.min(Matriz_Datos), end_type='num', end_value=np.max(Matriz_Datos), color = 'FA0000')
            valuef = 'B3:'+str(get_column_letter(NumCol+2))+str(NumRow+3)                
            sheet.conditional_formatting.add(valuef, rule)
            

            wb2.create_sheet()

            thin = Side(border_style="thin", color="FFFFFF") #color="CCFFFF"
            border = Border(left=thin, right=thin, top=thin, bottom=thin)
            
            sheet1 = wb2["Sheet1"]

            cti = sheet1.cell(row = 1, column = 1)
            cti.value = 'Etiquetas de fila'
            cti.alignment = Alignment(horizontal="center",
                                      vertical="center")
            fille = GradientFill(stop=("CCCCCC", "CCCCCC"))            
            cti.fill = fille
            cti.border = border

            print("Test4")
            

            cti = sheet1.cell(row = 1, column = 2)
            cti.value = 'Promedio de CAUDAL (l/s)'
            cti.alignment = Alignment(horizontal="center",
                                      vertical="center")
            fille = GradientFill(stop=("CCCCCC", "CCCCCC"))            
            cti.fill = fille
            cti.border = border

            print("Test5")

            try:
                for i in range(0,NumCol):
                    cf = sheet1.cell(row = 2+i, column = 1)
                    valuef = '=Sheet!'+str(get_column_letter(2+i))+'2'
                    cf.value = valuef
                    cf.alignment = Alignment(horizontal="center",
                                            vertical="center")
            except Exception as e:
                print(e)
            
                
            CantiRow2 = NumCol+1
            
            cti = sheet1.cell(1+CantiRow2, column = 1)
            cti.value = 'Total general'
            cti.alignment = Alignment(horizontal="center",
                                      vertical="center")
            cti.fill = fille
            cti.border = border

            sheet1.auto_filter.ref = "A1"

            cti = sheet1.cell(1, column = 4)
            cti.value = 'Mes'
            cti.alignment = Alignment(horizontal="center",
                                      vertical="center")
            cti.fill = fille
            cti.border = border

            cti = sheet1.cell(1, column = 5)
            cti.value = 'Promedio (l/s)'
            cti.alignment = Alignment(horizontal="center",
                                      vertical="center")
            cti.fill = fille
            cti.border = border

            
            sheet1.column_dimensions[List[0]].width = 12
            sheet1.column_dimensions[List[1]].width = 16

            try:
                for i in range(0,NumCol+1):
                    cf = sheet1.cell(row = 2+i, column = 2)
                    valuef = '=Sheet!'+str(get_column_letter(2+i))+str(NumRow+3)
                    cf.value = valuef
                    cf.alignment = Alignment(horizontal="center",
                                            vertical="center")
            except Exception as e:
                print(e)

            Fechas_x = np.zeros(NumCol, dtype = object)

            print("Test6")

            try:
                for i in range(0, NumCol):
                    cti = sheet.cell(2, column = 2+i)
                    Fechas_x[i] = cti.value
                    Fechas_x[i] = Fechas_x[i].replace('/','-')

            except Exception as e:
                print(e)
            
            Fechas_x = np.array(Fechas_x)
            x = [datetime.datetime.strptime(date, "%d-%m-%Y").date() for date in Fechas_x]
            

            print("Test7")

            X_month = np.zeros(len(x))

            for i in range(0, len(x)):
                X_month[i] = x[i].month

       
            
            Num_Mounth = str(int(X_month[0]))+";"
            for i in range(1, len(x)):
                if X_month[i-1] != X_month[i]:
                    Num_Mounth=Num_Mounth+str(int(X_month[i]))+";"
            

            Num_Mounth = str(Num_Mounth).split(';')


            NumMounths = np.zeros(len(Num_Mounth)-1)

            for i in range(0, len(Num_Mounth)-1):
                NumMounths[i] = int(Num_Mounth[i])



            Posi_Mounths = np.zeros(len(NumMounths))

            for i in range(0, len(NumMounths)):
                
                PosiMounths = np.array(np.where(X_month==NumMounths[i]))
                PosiMounths = np.array(PosiMounths[0])
                Posi_Mounths[i] = PosiMounths[len(PosiMounths)-1]


            cf = sheet1.cell(row = 2, column = 5)
            valuef = '=AVERAGE(B2:B'+str(int(Posi_Mounths[0]+2))+')'
            cf.value = valuef
            cf.alignment = Alignment(horizontal="center",
                                    vertical="center")

            for i in range(1, len(Posi_Mounths)):
                cf = sheet1.cell(row = 2+i, column = 5)
                valuef = '=AVERAGE(B'+str(int(Posi_Mounths[i-1]+3))+':B'+str(int(Posi_Mounths[i]+2))+')'
                cf.value = valuef
                cf.alignment = Alignment(horizontal="center",
                                    vertical="center")
            
            
            print("Test8")

            Fec = fechas[0].split('/')

            Mounth_Date = np.zeros(len(Posi_Mounths), dtype = object)

            for i in range(0,len(Posi_Mounths)): 
                date_mounth = str(int(NumMounths[i]))+"/"+str(int(Fec[2]))
                cti = sheet1.cell(row = 2+i, column = 4)
                cti.value = date_mounth


            valuef = 'B2:B'+str(NumCol+2)                
            sheet1.conditional_formatting.add(valuef, rule)


            wb2.create_sheet()

            thin = Side(border_style="thin", color="FFFFFF") #color="CCFFFF"
            border = Border(left=thin, right=thin, top=thin, bottom=thin)

            print("Test9")

            sheet2 = wb2["Sheet2"]

            cx = sheet2.cell(row = 1, column = 1)
            cx.value = 'Log Time - Days'

            cx = sheet2.cell(row = 1, column = 2)
            cx.value = 'Log Time - Hours'

            cx = sheet2.cell(row = 1, column = 3)
            cx.value = 'OCM 1 - Flow (l/s)'

            
            for i in range(0, len(result)):
                for j in range(0,288):                
                    cf = sheet2.cell(row = 2+j+288*i, column = 1)
                    valuef = '=Sheet!'+str(get_column_letter(2+i))+str(2)
                    cf.value = valuef
                    cf.alignment = Alignment(horizontal="center",
                                            vertical="center")

                
                    cf = sheet2.cell(row = 2+j+288*i, column = 2)
                    
                    valuef = '=Sheet!'+str(get_column_letter(1))+str(3+j)
                    cf.value = valuef
                    cf.alignment = Alignment(horizontal="center",
                                            vertical="center")
                    cf.number_format = numbers.FORMAT_DATE_TIME3

            for i in range(0, len(result)):
                for j in range(0,288):
                    cf = sheet2.cell(row = 2+j+288*i, column = 3)
                    valuef = '=Sheet!'+str(get_column_letter(2+i))+str(3+j)
                    cf.value = valuef
                    cf.alignment = Alignment(horizontal="center",
                                            vertical="center")

                
            

            print("Test10")

            
 
            wb2.save(nombre)



            
            mesa="Exportado correctamente en el formato excel (.xlsx)"
            msg = QMessageBox()
            msg.setIconPixmap(QPixmap('check.png'))
                        
            msg.setText("Excelente :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
            

            


        except PermissionError:
            
            mesa="Se ha denegado el permiso, cerrar el excel a reemplazar"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()

        except Exception as e:
            print(e)
            mesa="No se puede hacer tratamiento a los datos. Revisar información"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()


    def graf(self):
        try:            
            self.w.clear()
            CantRow=int(self.table.rowCount())
            

            try:
                Fecha_cero = minvalue
                Fecha_cero = str(Fecha_cero).split('-')
                Fecha_cero = Fecha_cero[2]+"-"+Fecha_cero[1]+"-"+Fecha_cero[0]

                Fecha_cero = days.Back(Fecha_cero)

                fir = np.zeros(CantRow, dtype = object)
                seco = np.zeros(CantRow, dtype = object)

                for i in range(0,CantRow):
                    fir[i], seco[i] = seconds.count4(str(self.table.item(i,1).text()))

                PosiInter1 = np.array(np.where(fir==Fecha_cero))
                PosiInter1 = np.array(PosiInter1[0])

                PosiInter1 = PosiInter1[0]

                

            except Exception as e:
                print(e)
                PosiInter1 = 0

            print(PosiInter1)

            try:
                Fecha_cero = maxvalue
                Fecha_cero = str(Fecha_cero).split('-')
                Fecha_cero = Fecha_cero[2]+"-"+Fecha_cero[1]+"-"+Fecha_cero[0]

                Fecha_cero = days.Back(Fecha_cero)

                fir = np.zeros(CantRow, dtype = object)
                seco = np.zeros(CantRow, dtype = object)

                for i in range(0,CantRow):
                    fir[i], seco[i] = seconds.count4(str(self.table.item(i,1).text()))

                PosiInter2 = np.array(np.where(fir==Fecha_cero))
                PosiInter2 = np.array(PosiInter2[0])

                PosiInter2 = PosiInter2[len(PosiInter2)-1]

                

            except Exception as e:
                print(e)
                PosiInter2 = CantRow-1

            print(PosiInter2)

            Epoch=np.zeros(CantRow)
            Log=np.zeros(CantRow)
            Level=np.zeros(CantRow)
            Volume=np.zeros(CantRow)
            Head=np.zeros(CantRow)
            Flow=np.zeros(CantRow)
            Dly=np.zeros(CantRow)
            Sys=np.zeros(CantRow)
            Rst=np.zeros(CantRow)
            

            for i in range(PosiInter1, PosiInter2+1):
                Epoch[i]=float(self.table.item(i,0).text())
                Level[i]=float(self.table.item(i,2).text())
                Volume[i]=float(self.table.item(i,3).text())
                Head[i]=float(self.table.item(i,4).text())
                Flow[i]=float(self.table.item(i,5).text())
                Dly[i]=float(self.table.item(i,6).text())
                Sys[i]=float(self.table.item(i,7).text())
                Rst[i]=float(self.table.item(i,8).text())

                reference=datetime.datetime(1969, 12, 31, 19, 0, 0) #------
                dia, mes, ano, hora, minuto, segundo = seconds.count(str(self.table.item(i,1).text()))                
                datonow=datetime.datetime(ano, mes, dia, hora, minuto, segundo)
                nowd=datonow-reference
                Log[i]=nowd.total_seconds()


            if (self.comboBox_2.currentText()=="Vol. 1 - Level (m)"):
                
                self.label_titlegrap.setText(self.name+"     Vol. 1 - Level (m)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Level[PosiInter1:PosiInter2+1], symbol='s', symbolSize=0.1, pen=(255,0,0),
                    symbolBrush=(255,0,0), symbolPen='r')
                
            elif (self.comboBox_2.currentText()=="Vol. 1 - Volume (m3)"):

                self.label_titlegrap.setText(self.name+"     Vol. 1 - Volume (m3)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Volume[PosiInter1:PosiInter2+1], symbol='s', symbolSize=0.1, pen=(0,255,0),
                    symbolBrush=(0,255,0), symbolPen='g')
                
            elif (self.comboBox_2.currentText()=="OCM 1 - Head (m)"):

                self.label_titlegrap.setText(self.name+"     OCM 1 - Head (m)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Head[PosiInter1:PosiInter2+1], symbol='s', symbolSize=0.1, pen=(0,0,255),
                    symbolBrush=(0,0,255), symbolPen='b')
                
            elif (self.comboBox_2.currentText()=="OCM 1 - Flow (litres/sec)"):

                self.label_titlegrap.setText(self.name+"     OCM 1 - Flow (litres/sec)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Flow[PosiInter1:PosiInter2+1], symbol='s', symbolSize=0.1, pen=(0,125,125),
                    symbolBrush=(0,125,125), symbolPen='b')
                
            elif (self.comboBox_2.currentText()=="OCM 1 - Dly Totaliser (m3)"):

                self.label_titlegrap.setText(self.name+"     OCM 1 - Dly Totaliser (m3)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Dly[PosiInter1:PosiInter2+1])
                
            elif (self.comboBox_2.currentText()=="OCM 1 - Sys Totaliser (m3)"):

                self.label_titlegrap.setText(self.name+"     OCM 1 - Sys Totaliser (m3)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Sys[PosiInter1:PosiInter2+1], symbol='s', symbolSize=0.1, pen=(125,125,0),
                    symbolBrush=(125,125,0), symbolPen='orange')
                
            elif (self.comboBox_2.currentText()=="OCM 1 - Rst Totaliser (m3)"):

                self.label_titlegrap.setText(self.name+"     OCM 1 - Rst Totaliser (m3)")
                self.w.plot(Log[PosiInter1:PosiInter2+1], Rst[PosiInter1:PosiInter2+1])
            
                
            

        except Exception as e:
            print(e)
            
#-------------- Code: 170123 --------------#
    def Opens(self):
        try:
            filename = QFileDialog.getOpenFileNames(None, 'Abrir Archivo', filter='Archivos de texto (*.prn;*.txt;*.csv);;Archivos de Excel (*.xlsx)')
            
            filename = filename[0]

            # Función para extraer la fecha del nombre de archivo
            #def get_date(filename):
            #    date_str = filename.split('-')[2]        
            #    date_obj = datetime.datetime.strptime(date_str, '%d%b%Y')
            #    return date_obj

            
            #filename = sorted(filename, key=get_date)

            #filename = sorted(filename, key=lambda x: datetime.datetime.strptime(x.split('-')[2], '%d%b%Y').replace(hour=int(x.split('-')[3][:2]), minute=int(x.split('-')[3][2:4]), second=int(x.split('-')[3][4:6])))
            filename = sorted(filename, key=lambda x: datetime.datetime.strptime((x.split('OCM-')[1]).split('-')[0], '%d%b%Y').replace(hour=int((x.split('OCM-')[1]).split('-')[1][:2]), minute=int((x.split('OCM-')[1]).split('-')[1][2:4]), second=int((x.split('OCM-')[1]).split('-')[1][4:6])))

            #print(filename)
            
            
            for i in range(0, len(filename)):
                self.Agrega(filename[i])

            self.progressBar.hide()
            
        except UserWarning:
            self.progressBar.hide()
            mesa="El archivo abierto no está completo o es incorrecto"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
            self.pushButton_generar.setEnabled(False)
            self.pushButton_fechas.setEnabled(False)
            self.pushButton_eliminar.setEnabled(False)
            self.pushButton_2.setEnabled(False)

        except Exception as e:
            self.progressBar.hide()
            mesa="El archivo abierto no contiene el formato establecido"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
            self.pushButton_generar.setEnabled(False)
            self.pushButton_fechas.setEnabled(False)
            self.pushButton_eliminar.setEnabled(False)
            self.pushButton_2.setEnabled(False)

#-------------- Code: 100622 --------------#
    def Agrega(self, filename):
        try:            
            if filename:
                Cadena=str(filename).split('/')
                self.name=Cadena[len(Cadena)-1]                    
                filename1=str(filename).split('.')
                long=len(filename1)
                
                CantRow=int(self.table.rowCount()) # Observing how many rows there are
                
                if filename1[long-1]=='csv' or filename1[long-1]=='txt' or filename1[long-1]=='prn':
           
                    try:
                        with open(filename, 'r') as File:
                            data = np.loadtxt(File,dtype='str', delimiter=",",skiprows=4)
                            self.n = len(data[:,0])
                            self.progressBar.show()
                            self.progressBar.setMaximum(self.n)
                            File.close()

                            
                            for i in range(0,self.n):
                                self.Dato_1=data[i,0]
                                self.Dato_2=data[i,1]
                                self.Dato_3=data[i,2]
                                self.Dato_4=data[i,3]
                                self.Dato_5=data[i,4]
                                self.Dato_6=data[i,5]
                                self.Dato_7=data[i,6]
                                self.Dato_8=data[i,7]
                                self.Dato_9=data[i,8]
                                self.progressBar.setValue(i+1)
                                
                                self.table.setRowCount(CantRow + i + 1)

                                self.table.setItem(CantRow + i, 0, QTableWidgetItem(self.Dato_1))
                                self.table.setItem(CantRow + i, 1, QTableWidgetItem(self.Dato_2))
                                self.table.setItem(CantRow + i, 2, QTableWidgetItem(self.Dato_3))
                                self.table.setItem(CantRow + i, 3, QTableWidgetItem(self.Dato_4))
                                self.table.setItem(CantRow + i, 4, QTableWidgetItem(self.Dato_5))
                                self.table.setItem(CantRow + i, 5, QTableWidgetItem(self.Dato_6))
                                self.table.setItem(CantRow + i, 6, QTableWidgetItem(self.Dato_7))
                                self.table.setItem(CantRow + i, 7, QTableWidgetItem(self.Dato_8))
                                self.table.setItem(CantRow + i, 8, QTableWidgetItem(self.Dato_9))


                            try:
                                self.pushButton_generar.setEnabled(True)
                                self.pushButton_fechas.setEnabled(True)                                
                                self.pushButton_2.setEnabled(True)
                                self.progressBar.hide()
                            except:
                                pass
                                
                                
                    except Exception as e:
                        self.progressBar.hide()
                        mesa="El archivo abierto no contiene el formato establecido"
                        msg = QMessageBox()
                        msg.setIcon(QMessageBox.Critical)
                        msg.setText("Error :")
                        msg.setInformativeText(mesa)
                        msg.setWindowTitle("Data Analytics - Controller")
                        msg.exec_()
                        self.pushButton_generar.setEnabled(False)
                        self.pushButton_fechas.setEnabled(False)                        
                        self.pushButton_2.setEnabled(False)
                        print(e)

                    
                    File.close()

                elif filename1[long-1]=='xlsx':
                    work_book=load_workbook(filename)                    
                    hoja_book=work_book.active                    
                    max_fila=hoja_book.max_row                    
                    self.progressBar.show()
                    self.progressBar.setMaximum(max_fila)
                    
                    try:
                        max_columna=hoja_book.max_column

                        if (max_columna == 9):

                            for j in range(2,max_fila+1):

                                celda_book_1=hoja_book.cell(j,1)
                                celda_book_2=hoja_book.cell(j,2)
                                celda_book_3=hoja_book.cell(j,3)
                                celda_book_4=hoja_book.cell(j,4)
                                celda_book_5=hoja_book.cell(j,5)
                                celda_book_6=hoja_book.cell(j,6)
                                celda_book_7=hoja_book.cell(j,7)
                                celda_book_8=hoja_book.cell(j,8)
                                celda_book_9=hoja_book.cell(j,9)

                                self.progressBar.setValue(j-2+1)

                                self.table.setRowCount(CantRow + j - 2 + 1)

                                self.table.setItem(CantRow + j - 2, 0, QTableWidgetItem(str(celda_book_1.value)))
                                self.table.setItem(CantRow + j - 2, 1, QTableWidgetItem(str(celda_book_2.value)))
                                self.table.setItem(CantRow + j - 2, 2, QTableWidgetItem(str(celda_book_3.value)))
                                self.table.setItem(CantRow + j - 2, 3, QTableWidgetItem(str(celda_book_4.value)))
                                self.table.setItem(CantRow + j - 2, 4, QTableWidgetItem(str(celda_book_5.value)))
                                self.table.setItem(CantRow + j - 2, 5, QTableWidgetItem(str(celda_book_6.value)))
                                self.table.setItem(CantRow + j - 2, 6, QTableWidgetItem(str(celda_book_7.value)))
                                self.table.setItem(CantRow + j - 2, 7, QTableWidgetItem(str(celda_book_8.value)))
                                self.table.setItem(CantRow + j - 2, 8, QTableWidgetItem(str(celda_book_9.value)))

                            try:
                                self.pushButton_generar.setEnabled(True)
                                self.pushButton_fechas.setEnabled(True)                                
                                self.pushButton_2.setEnabled(True)
                                self.progressBar.hide()
                            except:
                                pass


                        else:
                            self.progressBar.hide()
                            mesa="El archivo abierto no contiene el formato establecido"
                            msg = QMessageBox()
                            msg.setIcon(QMessageBox.Critical)
                            msg.setText("Error :")
                            msg.setInformativeText(mesa)
                            msg.setWindowTitle("Data Analytics - Controller")
                            msg.exec_()
                            self.pushButton_generar.setEnabled(False)
                            self.pushButton_fechas.setEnabled(False)                            
                            self.pushButton_2.setEnabled(False)

                    except Exception as e:
                        self.progressBar.hide()
                        mesa="El archivo abierto no contiene el formato establecido"
                        msg = QMessageBox()
                        msg.setIcon(QMessageBox.Critical)
                        msg.setText("Error :")
                        msg.setInformativeText(mesa)
                        msg.setWindowTitle("Data Analytics - Controller")
                        msg.exec_()
                        self.pushButton_generar.setEnabled(False)
                        self.pushButton_fechas.setEnabled(False)                        
                        self.pushButton_2.setEnabled(False)
                        print(e)
                            
                    
                    
            self.progressBar.hide()     

        except UserWarning:
            self.progressBar.hide()
            mesa="El archivo abierto no está completo o es incorrecto"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
            self.pushButton_generar.setEnabled(False)
            self.pushButton_fechas.setEnabled(False)            
            self.pushButton_2.setEnabled(False)

        except Exception as e:
            self.progressBar.hide()
            mesa="El archivo abierto no contiene el formato establecido"
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error :")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Data Analytics - Controller")
            msg.exec_()
            self.pushButton_generar.setEnabled(False)
            self.pushButton_fechas.setEnabled(False)            
            self.pushButton_2.setEnabled(False)                

    def Nuev(self):
        try:
            self.close()
            self.ui = MyApp()
            self.ui.show()
        except:
            pass

    def mouseMoved(self, evt):
        pos = evt
        if self.plt_wid.sceneBoundingRect().contains(pos):
            mousePoint = self.plt_wid.vb.mapSceneToView(pos)
            mx = np.array([abs(i-mousePoint.x()) for i in self.plotx])
            index = mx.argmin()
            if index >= 0 and index < len(self.plotx):
                self.cursorlabel.setText(
                    "<span style='font-size: 12pt'>x={:0.1f}, \
                     <span style='color: red'>y={:0.4f}</span>".format(
                     self.plotx[index], self.ploty[index])
                     )
            self.vLine.setPos(self.plotx[index])
            self.hLine.setPos(self.ploty[index])

    def Acer(self):
        try:
            Ui_Acerca(self).exec_()          

        except Exception as e:
            print(e)

#-------------- Code: 110622 --------------#
            
    def fecha(self):
        try:
            CantRow=int(self.table.rowCount())
            fechas = np.zeros(CantRow, dtype = object)
            for i in range(0,CantRow):
                fechas[i] = datetime.datetime.strptime(seconds.count2(str(self.table.item(i,1).text())), '%d/%m/%Y')

            global maxdate, mindate

            maxdate = max(fechas)
            mindate = min(fechas)
            
            Ui_Calendario(self).exec_()          

        except Exception as e:
            print(e)            
























if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('0.ico'))
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
















