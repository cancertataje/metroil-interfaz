import datetime

A = ['C:/Users/cance/Downloads/Vol-OCM-28nov2022-180501.csv', 
     'C:/Users/cance/Downloads/Vol-OCM-18oct2022-113727.csv', 
     'C:/Users/cance/Downloads/Vol-OCM-15apr2021-134727.csv']

# Función para extraer la fecha del nombre de archivo
def get_date(filename):
    date_str = filename.split('-')[2]    
    date_obj = datetime.datetime.strptime(date_str, '%d%b%Y')
    return date_obj


# Ordenamos el vector A por fecha
A_sorted = sorted(A, key=get_date)

print(A_sorted)
