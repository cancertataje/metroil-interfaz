import xlsxwriter

workbook = xlsxwriter.Workbook('conditional_format.xlsx')
worksheet1 = workbook.add_worksheet()
worksheet2 = workbook.add_worksheet()
worksheet3 = workbook.add_worksheet()
worksheet4 = workbook.add_worksheet()
worksheet5 = workbook.add_worksheet()
worksheet6 = workbook.add_worksheet()
worksheet7 = workbook.add_worksheet()
worksheet8 = workbook.add_worksheet()
worksheet9 = workbook.add_worksheet()




###############################################################################
#
# Example 8.
#
caption = 'Examples of data bars.'

worksheet8.write('A1', caption)

worksheet8.write('B2', "Default data bars")
worksheet8.write('D2', "Bars only")
worksheet8.write('F2', "With user color")
worksheet8.write('H2', "Solid bars")
worksheet8.write('J2', "Right to left")
worksheet8.write('L2', "Excel 2010 style")
worksheet8.write('N2', "Negative same as positive")

data = range(1, 13)
for row, row_data in enumerate(data):
    worksheet8.write(row + 2, 1, row_data)
    worksheet8.write(row + 2, 3, row_data)
    worksheet8.write(row + 2, 5, row_data)
    worksheet8.write(row + 2, 7, row_data)
    worksheet8.write(row + 2, 9, row_data)

data = [-1, -2, -3, -2, -1, 0, 1, 2, 3, 2, 1, 0]
for row, row_data in enumerate(data):
    worksheet8.write(row + 2, 11, row_data)
    worksheet8.write(row + 2, 13, row_data)

worksheet8.conditional_format('B3:B14', {'type': 'data_bar'})

worksheet8.conditional_format('D3:D14', {'type': 'data_bar',
                                         'bar_only': True})

worksheet8.conditional_format('F3:F14', {'type': 'data_bar',
                                         'bar_color': '#63C384'})

worksheet8.conditional_format('H3:H14', {'type': 'data_bar',
                                         'bar_solid': True})

worksheet8.conditional_format('J3:J14', {'type': 'data_bar',
                                         'bar_direction': 'right'})

worksheet8.conditional_format('L3:L14', {'type': 'data_bar',
                                         'data_bar_2010': True})

worksheet8.conditional_format('N3:N14', {'type': 'data_bar',
                                         'bar_negative_color_same': True,
                                         'bar_negative_border_color_same': True})



workbook.close()
