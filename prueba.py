from datetime import datetime, timedelta
from random import shuffle, randint
import matplotlib.dates as dates
import matplotlib.pyplot as plt

formato = dates.DateFormatter("%H:%M")
dias = [datetime.now() + timedelta(hours=x) for x in range(10)]
precios =[randint(1,100) for x in enumerate(dias)]
shuffle(precios)

plt.plot(dias, precios)
plt.gca().xaxis.set_major_formatter(formato)
plt.show()
