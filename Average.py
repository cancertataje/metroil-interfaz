import numpy as np

def Group(A):
    A = np.array(A)
    Zero = np.array(np.where(A == 0))
    Zero = np.array(Zero[0])

    if (len(A) > len(Zero)):

        Average = sum(A)/(len(A)-len(Zero))

    else:

        Average = 0

    return Average
